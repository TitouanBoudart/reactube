import React, {useState} from 'react';
import Container from 'react-bootstrap/Container';
import MyNav from './components/MyNav';
import Result from './components/Result'
import DetailedVideo from "./components/DetailedVideo";
import DetailedChannel from "./components/DetailedChannel"
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
  const [results, setResults] = useState([]);
  const [selectedVideo, selectVideo] = useState({});
  const [selectedChannel, selectChannel] = useState({});
  const [type, changeType] = useState("video");

  return (
    <Container className="p-3">
        <Router>
            <MyNav onResults={setResults} changeType={changeType} type={type}/>

            <Switch>
                <Route path="/videos/:videoId">
                    <DetailedVideo id={selectedVideo.id}
                                   snippet={selectedVideo.snippet} player={selectedVideo.player}
                                   statistics={selectedVideo.statistics} />
                </Route>
                <Route path="/search/:type/:search">
                        <span>type : {type}</span>
                        <>
                        {
                            results.map(v => {
                                let typeResult=type;
                                return( <Result queryResult={v} type={typeResult} selectVideo={selectVideo} selectChannel={selectChannel}/>)
                            })
                        }
                        </>
                </Route>
                <Route path="/channel/:channelid">
                   <DetailedChannel id={selectedChannel.id} snippet={selectedChannel.snippet} statistics={selectedChannel.statistics}/>
                </Route>
                <Route path="/">
                    Merci d'effectuer une recherche...
                </Route>
            </Switch>
        </Router>
    </Container>
);
};


export default App;
