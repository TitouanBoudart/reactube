import {Row,Col,Image, Card} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";

const Channel = (props) =>{
    
    const selectChannel = props.selectChannel;
    const {channelId, channelTitle,description,thumbnails} = props.queryResult.snippet
    const history = useHistory();

    const search = async () => {
        
        const resp = await api.get('/channels', 
        { params: {
            id: channelId,
            part: 'snippet,statistics,contentDetails',
        }}
        );
        console.log('Received', resp.data.items);
        selectChannel(resp.data.items[0]);
        history.push(`/channel/${channelId}`);
    };

    return (
        
        <Card>
        <Card.Body>
        <Row>
        <Col md={2}>
        <Image src={thumbnails.high.url} fluid />
        </Col>
        <Col>
        <Card.Title onClick={search}>{channelTitle}</Card.Title>
        <Card.Text>
            {description}
        </Card.Text>
        </Col>
        
        
        </Row>
        </Card.Body>
        </Card>
        )
        
} 
export default Channel;