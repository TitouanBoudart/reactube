import Navbar from "react-bootstrap/Navbar";
import { Nav } from "react-bootstrap";
import SearchBar from './SearchBar';
import {useHistory} from "react-router-dom";

const MyNav = ({ onResults , changeType , type}) => {
  const history = useHistory();
  return (<Navbar bg="dark" variant="dark">
    <Navbar.Brand onClick={ () => history.push("/")}>Reactube</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link onClick={ () => {changeType("video"); history.push("/search/videos")}}>Videos</Nav.Link>
      <Nav.Link onClick={ () => {changeType("channel"); history.push("/search/channel")}}>Channels</Nav.Link>
    </Nav>
    <SearchBar onResults={onResults} type={type}/>
  </Navbar>);
}
export default MyNav;