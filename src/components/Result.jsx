import Video from './Video'
import Channel from './Channel'

const Result = (props) =>{
    const {queryResult, type, selectVideo, selectChannel} = props;
    
    console.log(type);
    if(type === "video"){
        return (
            <div>
                Video
                <Video queryResult={queryResult} selectVideo={selectVideo} />
            </div>
        );
    }
    else{

        return(
            <div>
                channel
                <Channel queryResult={queryResult} selectChannel={selectChannel}/>
            </div>
        );
    }
        
} 
export default Result;