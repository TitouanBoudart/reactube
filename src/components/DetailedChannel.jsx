import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import {useHistory} from "react-router-dom";

const DetailedChannel = ({id, snippet, statistics}) => {

    const {title,description, thumbnails} = snippet;
    const {viewCount, videoCount, subscriberCount} = statistics;
    const history = useHistory();

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Button variant="danger" style={{float: 'right'}} onClick={history.goBack}>Retour</Button>
            <Row>
                <Col md={3}>
                    <Image src={thumbnails.high.url} fluid />
                </Col>
                <Col>
                    <h2>{title}</h2>
                    <h5 className="mb-2 text-muted">
                       {description}
                    </h5>
                    <h6 className="mb-2 text-muted">
                        Views : {viewCount}
                    </h6>
                    <h6 className="mb-2 text-muted">
                        Videos : {videoCount}
                    </h6>
                    <h6 className="mb-2 text-muted">
                        Subscribers : {subscriberCount}
                    </h6>
                </Col>
            </Row>
        </Card.Body>
    </Card>);
}

export default DetailedChannel;
