import {Row,Col,Image, Card} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";

const Video = (props) =>{

    
    
    //const {selectVideo, thumbnail, description, title, channelTitle, videoId, publishTime} = props;
    console.log(props);
    const {
        title, description,
        channelTitle, publishTime
    } = props.queryResult.snippet;

    const videoId = props.queryResult.id.videoId;
    const thumbnail = props.queryResult.snippet.thumbnails.high.url;
    const selectVideo = props.selectVideo;
    const history = useHistory();

    const search = async () => {
        const resp = await api.get('/videos', 
        { params: {
            id: videoId,
            part: 'snippet,statistics,contentDetails,player,recordingDetails,topicDetails',
        }}
        );
        console.log('Received', resp.data.items);
        selectVideo(resp.data.items[0]);
        history.push(`/videos/${videoId}`);
    };

    return (
        
        <Card>
        <Card.Body>
        <Row>
        <Col md={4}>
        <Image src={thumbnail} fluid />
        </Col>
        <Col>
        <Card.Title onClick={search}>{title}</Card.Title>
        <Card.Subtitle>{channelTitle}</Card.Subtitle>
        <Card.Text>
            {description}
        </Card.Text>
        <Card.Text className="mb-1 text-muted">
            {publishTime}
        </Card.Text>
        </Col>
        
        
        </Row>
        </Card.Body>
        </Card>
        )
} 
export default Video;