import axios from 'axios'
const key = 'AIzaSyCnYIEjehsw3-b93UuSTCqCymSJcSKgncQ'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})
export default fetcher;